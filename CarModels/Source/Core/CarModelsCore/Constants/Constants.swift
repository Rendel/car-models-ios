//
//  Constants.swift
//  CarModels
//
//  Created by Giorgi Iashvili on 9/22/19.
//  Copyright © 2019 Giorgi Iashvili. All rights reserved.
//

import Foundation

struct Constants {
    
    struct General {
        
    }
    
    struct Network {
        
        static let auto1ApiVersion: String = "v1"
        static let auto1BaseApiUrl: String = "http://api-aws-eu-qa-1.auto1-test.com/"
        static var auto1ApiUrl: String { get { return self.auto1BaseApiUrl + self.auto1ApiVersion } }
        
        static var auto1ApiKey_key = "wa_key"
        static var auto1ApiKey_value = "coding-puzzle-client-449cc9d"
        
    }
    
    struct Animation {
        
        static let defaultDuration: TimeInterval = 0.27
        
    }
    
}

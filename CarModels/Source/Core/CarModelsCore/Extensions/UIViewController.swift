//
//  UIViewController.swift
//  CarModels
//
//  Created by Giorgi Iashvili on 9/22/19.
//  Copyright © 2019 Giorgi Iashvili. All rights reserved.
//

import UIKit
import RDExtensionsSwift

extension UIViewController {
    
    public class func loadFromStoryboard() -> Self
    {
        return self.load(from: self.className.replacingOccurrences(of: "NavigationController", with: "").replacingOccurrences(of: "ViewController", with: "").replacingOccurrences(of: "Controller", with: ""))!
    }
    
    func startLoader()
    {
        self.view.startLoader()
    }
    
    func stopLoader()
    {
        self.view.stopLoader()
    }
    
}

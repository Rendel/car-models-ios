//
//  UIView.swift
//  CarModels
//
//  Created by Giorgi Iashvili on 9/22/19.
//  Copyright © 2019 Giorgi Iashvili. All rights reserved.
//

import UIKit
import SVProgressHUD

extension UIView {
    
    func startLoader()
    {
        SVProgressHUD.show()
        self.isUserInteractionEnabled = false
    }
    
    func stopLoader()
    {
        SVProgressHUD.dismiss()
        self.isUserInteractionEnabled = true
    }
    
}

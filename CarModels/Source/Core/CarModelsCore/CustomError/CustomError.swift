//
//  CustomError.swift
//  CarModels
//
//  Created by Giorgi Iashvili on 9/22/19.
//  Copyright © 2019 Giorgi Iashvili. All rights reserved.
//

import Foundation

class CustomError {
    
    let localizedDescription: String
    
    init(localizedDescription: String)
    {
        self.localizedDescription = localizedDescription
    }
    
}

extension CustomError: Error {
}

extension CustomError: LocalizedError {
    
    var errorDescription: String? { return self.localizedDescription }
    
}

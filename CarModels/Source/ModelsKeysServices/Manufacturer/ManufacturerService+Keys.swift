//
//  ManufacturerService+Keys.swift
//  CarModels
//
//  Created by Giorgi Iashvili on 9/22/19.
//  Copyright © 2019 Giorgi Iashvili. All rights reserved.
//

extension ManufacturerService {
    
    class Keys {
        
        class ServicePaths {
            
            static let loadManufacturers = "/car-types/manufacturer"
            
        }
        
        class Parameters {
            
            static let page = "page"
            static let pageSize = "pageSize"
            
        }
        
        class Parsing {
            
            static let wkda = "wkda"
            
        }
        
    }
    
}

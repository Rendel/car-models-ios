//
//  ManufacturerService.swift
//  CarModels
//
//  Created by Giorgi Iashvili on 9/22/19.
//  Copyright © 2019 Giorgi Iashvili. All rights reserved.
//

import Foundation

class ManufacturerService {
    
    func loadManufacturers(page: Int, pageSize: Int, success: @escaping ([Manufacturer]) -> Void, fail: @escaping (Error) -> Void)
    {
        Network.jsonRequest(url: Constants.Network.auto1ApiUrl, path: Keys.ServicePaths.loadManufacturers, query: [
            Keys.Parameters.page                        :       page.toString,
            Keys.Parameters.pageSize                    :       pageSize.toString,
            Constants.Network.auto1ApiKey_key           :       Constants.Network.auto1ApiKey_value
        ], json: { json in
            DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
                let manufacturers = json[Keys.Parsing.wkda].dictionaryValue.map { Manufacturer(id: $0.key, name: $0.value.stringValue) }
                DispatchQueue.main.async {
                    success(manufacturers)
                }
            }
        }, fail: fail)
    }
    
}

//
//  Manufacturer.swift
//  CarModels
//
//  Created by Giorgi Iashvili on 9/22/19.
//  Copyright © 2019 Giorgi Iashvili. All rights reserved.
//

import SwiftyJSON

class Manufacturer {
    
    var id: String
    var name: String
    
    init(id: String, name: String)
    {
        self.id = id
        self.name = name
    }
    
}

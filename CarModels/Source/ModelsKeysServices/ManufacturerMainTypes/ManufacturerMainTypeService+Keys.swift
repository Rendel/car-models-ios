//
//  ManufacturerMainTypeService+Keys.swift
//  CarModels
//
//  Created by Giorgi Iashvili on 9/22/19.
//  Copyright © 2019 Giorgi Iashvili. All rights reserved.
//

extension ManufacturerMainTypeService {
    
    class Keys {
        
        class ServicePaths {
            
            static let loadManufacturerMainTypes = "/car-types/main-types"
            
        }
        
        class Parameters {
            
            static let manufacturer = "manufacturer"
            
        }
        
        class Parsing {
            
            static let wkda = "wkda"
            
        }
        
    }
    
}

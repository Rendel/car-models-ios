//
//  ManufacturerMainTypes.swift
//  CarModels
//
//  Created by Giorgi Iashvili on 9/22/19.
//  Copyright © 2019 Giorgi Iashvili. All rights reserved.
//

import Foundation

class ManufacturerMainType {
    
    var name: String
    
    init(name: String)
    {
        self.name = name
    }
    
}

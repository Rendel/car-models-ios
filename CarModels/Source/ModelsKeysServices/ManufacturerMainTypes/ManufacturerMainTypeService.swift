//
//  ManufacturerMainTypeService.swift
//  CarModels
//
//  Created by Giorgi Iashvili on 9/22/19.
//  Copyright © 2019 Giorgi Iashvili. All rights reserved.
//

import Foundation

class ManufacturerMainTypeService {
    
    func loadManufacturerMainTypes(manufacturerId: String, success: @escaping ([ManufacturerMainType]) -> Void, fail: @escaping (Error) -> Void)
    {
        Network.jsonRequest(url: Constants.Network.auto1ApiUrl, path: Keys.ServicePaths.loadManufacturerMainTypes, query: [
            Keys.Parameters.manufacturer                :       manufacturerId,
            Constants.Network.auto1ApiKey_key           :       Constants.Network.auto1ApiKey_value
        ], json: { json in
            DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
                let manufacturerMainTypes = json[Keys.Parsing.wkda].dictionaryValue.values.map { ManufacturerMainType(name: $0.stringValue) }
                DispatchQueue.main.async {
                    success(manufacturerMainTypes)
                }
            }
        }, fail: fail)
    }
    
}

//
//  AppDelegate.swift
//  CarModels
//
//  Created by Giorgi Iashvili on 9/22/19.
//  Copyright © 2019 Giorgi Iashvili. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder {
    
    var window: UIWindow?
    
    private func setupWindow()
    {
        self.window = UIWindow()
        let appRouter = AppRouter()
        appRouter.load()
        self.window?.rootViewController = appRouter.navigationController
        self.window?.makeKeyAndVisible()
    }
    
}

// MARK: - UIApplicationDelegate
extension AppDelegate: UIApplicationDelegate {
    
    func applicationDidFinishLaunching(_ application: UIApplication)
    {
        self.setupWindow()
    }
    
}

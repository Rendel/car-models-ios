//
//  ManufacturerTableViewCell.swift
//  CarModels
//
//  Created by Giorgi Iashvili on 9/22/19.
//  Copyright © 2019 Giorgi Iashvili. All rights reserved.
//

import UIKit
import RDExtensionsSwift

class ManufacturerTableViewCell: UITableViewCell, Reusable {
    
    @IBOutlet private weak var lblName: UILabel!
    
    func fill(withManufacturer manufacturer: Manufacturer, indexPath: IndexPath)
    {
        self.lblName.text = manufacturer.name
        self.lblName.textColor = indexPath.row % 2 == .zero ? .black : .white
        
        self.contentView.backgroundColor = indexPath.row % 2 == .zero ? .white : .lightGray
    }
    
}

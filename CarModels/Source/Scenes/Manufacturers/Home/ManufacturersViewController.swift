//
//  ManufacturersViewController.swift
//  CarModels
//
//  Created by Giorgi Iashvili on 9/22/19.
//  Copyright © 2019 Giorgi Iashvili. All rights reserved.
//

import UIKit

class ManufacturersViewController: UIViewController {
    
    private var viewModel: ManufacturersViewModelType!
    private var router: ManufacturersRouter!
    
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var viewActivityIndivator: UIActivityIndicatorView!
    
    private var page = 0
    
    static func load(withViewModel viewModel: ManufacturersViewModelType, router: ManufacturersRouter) -> Self
    {
        let vc = self.loadFromStoryboard()
        vc.viewModel = viewModel
        vc.router = router
        return vc
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.setupTableView()
        
        self.startLoader()
        self.loadManufacturers()
    }
    
    private func setupTableView()
    {
        self.tableView.register(UINib(nibName: ManufacturerTableViewCell.className, bundle: nil), forCellReuseIdentifier: ManufacturerTableViewCell.reuseIdentifier)
    }
    
    private func loadManufacturers()
    {
        self.viewModel.inputs.loadManufacturers(page: self.page, success: { [weak self] in
            self?.stopLoader()
            self?.viewActivityIndivator.stopAnimating()
            self?.tableView.reloadData()
        }, fail: { [weak self] error in
            self?.stopLoader()
            self?.viewActivityIndivator.stopAnimating()
            self?.presentAlert(withError: error)
        })
    }
    
    private func presentAlert(withError error: Error)
    {
        let alert = UIAlertController(message: error.localizedDescription, actions: [], cancelButtonTitle: "Close")
        self.present(alert, animated: true)
    }
    
}

// MARK: - UITableViewDataSource implementation
extension ManufacturersViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return self.viewModel.outputs.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.viewModel.outputs.numberOfItems(inSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: ManufacturerTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        if let item = self.viewModel.outputs.item(atIndexPath: indexPath)
        {
            cell.fill(withManufacturer: item, indexPath: indexPath)
        }
        return cell
    }
    
}

// MARK: - UITableViewDelegate implementation
extension ManufacturersViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if let item = self.viewModel.outputs.item(atIndexPath: indexPath)
        {
            self.router.loadMainTypes(withManufacturer: item)
        }
    }
    
}

// MARK: - UIScrollViewDelegate implementation
extension ManufacturersViewController {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        let endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height
        if(!self.viewActivityIndivator.isAnimating && endScrolling + 1 >= scrollView.contentSize.height)
        {
            self.page += 1
            self.viewActivityIndivator.startAnimating()
            self.loadManufacturers()
        }
    }
    
}

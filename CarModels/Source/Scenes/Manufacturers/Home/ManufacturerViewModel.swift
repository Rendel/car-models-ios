//
//  ManufacturersViewModel.swift
//  CarModels
//
//  Created by Giorgi Iashvili on 9/22/19.
//  Copyright © 2019 Giorgi Iashvili. All rights reserved.
//

import Foundation

protocol ManufacturersViewModelType {
    
    var inputs: ManufacturersViewModelInputs { get }
    var outputs: ManufacturersViewModelOutputs { get }
    
}

protocol ManufacturersViewModelInputs {
    
    func loadManufacturers(page: Int, success: @escaping () -> Void, fail: @escaping (Error) -> Void)
    
}

protocol ManufacturersViewModelOutputs {
    
    func numberOfSections() -> Int
    func numberOfItems(inSection section: Int) -> Int
    func item(atIndexPath indexPath: IndexPath) -> Manufacturer?
    
}

class ManufacturersViewModel {
    
    private let service: ManufacturerService
    
    private var manufacturers: [Manufacturer] = []
    private var dataSource: [[Manufacturer]] { get { return [self.manufacturers] } }
    
    private let pageSize: Int
    
    init(withService service: ManufacturerService, pageSize: Int = 15)
    {
        self.service = service
        self.pageSize = pageSize
    }
    
}

// MARK: - ManufacturersViewModelInputs implementation
extension ManufacturersViewModel: ManufacturersViewModelInputs {
    
    func loadManufacturers(page: Int, success: @escaping () -> Void, fail: @escaping (Error) -> Void)
    {
        self.service.loadManufacturers(page: page, pageSize: self.pageSize, success: { [weak self] manufacturers in
            if(page == .zero)
            {
                self?.manufacturers = manufacturers
            }
            else
            {
                self?.manufacturers += manufacturers
            }
            success()
        }, fail: fail)
    }
    
}

// MARK: - ManufacturersViewModelOutputs implementation
extension ManufacturersViewModel: ManufacturersViewModelOutputs {
    
    func numberOfSections() -> Int
    {
        return self.dataSource.count
    }
    
    func numberOfItems(inSection section: Int) -> Int
    {
        return self.dataSource[section].count
    }
    
    func item(atIndexPath indexPath: IndexPath) -> Manufacturer?
    {
        return self.dataSource[indexPath]
    }
    
}

// MARK: - ManufacturersViewModelType implementation
extension ManufacturersViewModel: ManufacturersViewModelType {
    
    var inputs: ManufacturersViewModelInputs { get { return self } }
    var outputs: ManufacturersViewModelOutputs { get { return self } }
    
}

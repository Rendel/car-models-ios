//
//  ManufacturerMainTypesViewController.swift
//  CarModels
//
//  Created by Giorgi Iashvili on 9/22/19.
//  Copyright © 2019 Giorgi Iashvili. All rights reserved.
//

import UIKit

class ManufacturerMainTypesViewController: UIViewController {
    
    private var viewModel: ManufacturerMainTypesViewModelType!
    private var router: ManufacturerMainTypesRouter!
    
    @IBOutlet private weak var tableView: UITableView!
    
    private var page = 0
    
    static func load(withViewModel viewModel: ManufacturerMainTypesViewModelType, router: ManufacturerMainTypesRouter) -> Self
    {
        let vc = self.loadFromStoryboard()
        vc.viewModel = viewModel
        vc.router = router
        return vc
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.title = self.viewModel.outputs.manufacturer.name
        
        self.setupTableView()
        
        self.startLoader()
        self.loadManufacturerMainTypes()
    }
    
    private func setupTableView()
    {
        self.tableView.register(UINib(nibName: ManufacturerMainTypeTableViewCell.className, bundle: nil), forCellReuseIdentifier: ManufacturerMainTypeTableViewCell.reuseIdentifier)
    }
    
    private func loadManufacturerMainTypes()
    {
        self.viewModel.inputs.loadManufacturerMainTypes(success: { [weak self] in
            self?.stopLoader()
            self?.tableView.reloadData()
        }, fail: { [weak self] error in
            self?.stopLoader()
            self?.presentAlert(withError: error)
        })
    }
    
    private func presentAlert(withError error: Error)
    {
        let alert = UIAlertController(message: error.localizedDescription, actions: [], cancelButtonTitle: "Close")
        self.present(alert, animated: true)
    }
    
}

// MARK: - UITableViewDataSource implementation
extension ManufacturerMainTypesViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return self.viewModel.outputs.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.viewModel.outputs.numberOfItems(inSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: ManufacturerMainTypeTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        if let item = self.viewModel.outputs.item(atIndexPath: indexPath)
        {
            cell.fill(withManufacturerMainType: item, indexPath: indexPath)
        }
        return cell
    }
    
}

// MARK: - UITableViewDelegate implementation
extension ManufacturerMainTypesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if let item = self.viewModel.outputs.item(atIndexPath: indexPath)
        {
            self.router.manufacturer(self.viewModel.outputs.manufacturer, didChooseMainType: item)
        }
    }
    
}

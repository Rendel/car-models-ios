//
//  ManufacturerMainTypesViewModel.swift
//  CarModels
//
//  Created by Giorgi Iashvili on 9/22/19.
//  Copyright © 2019 Giorgi Iashvili. All rights reserved.
//

import Foundation

protocol ManufacturerMainTypesViewModelType {
    
    var inputs: ManufacturerMainTypesViewModelInputs { get }
    var outputs: ManufacturerMainTypesViewModelOutputs { get }
    
}

protocol ManufacturerMainTypesViewModelInputs {
    
    func loadManufacturerMainTypes(success: @escaping () -> Void, fail: @escaping (Error) -> Void)
    
}

protocol ManufacturerMainTypesViewModelOutputs {
    
    var manufacturer: Manufacturer { get }
    
    func numberOfSections() -> Int
    func numberOfItems(inSection section: Int) -> Int
    func item(atIndexPath indexPath: IndexPath) -> ManufacturerMainType?
    
}

class ManufacturerMainTypesViewModel {
    
    private let service: ManufacturerMainTypeService
    let manufacturer: Manufacturer
    
    private var manufacturerMainTypes: [ManufacturerMainType] = []
    private var dataSource: [[ManufacturerMainType]] { get { return [self.manufacturerMainTypes] } }
    
    init(withService service: ManufacturerMainTypeService, manufacturer: Manufacturer)
    {
        self.service = service
        self.manufacturer = manufacturer
    }
    
}

// MARK: - ManufacturerMainTypesViewModelInputs implementation
extension ManufacturerMainTypesViewModel: ManufacturerMainTypesViewModelInputs {
    
    func loadManufacturerMainTypes(success: @escaping () -> Void, fail: @escaping (Error) -> Void)
    {
        self.service.loadManufacturerMainTypes(manufacturerId: self.manufacturer.id, success: { [weak self] manufacturerMainTypes in
            self?.manufacturerMainTypes = manufacturerMainTypes
            success()
        }, fail: fail)
    }
    
}

// MARK: - ManufacturerMainTypesViewModelOutputs implementation
extension ManufacturerMainTypesViewModel: ManufacturerMainTypesViewModelOutputs {
    
    func numberOfSections() -> Int
    {
        return self.dataSource.count
    }
    
    func numberOfItems(inSection section: Int) -> Int
    {
        return self.dataSource[section].count
    }
    
    func item(atIndexPath indexPath: IndexPath) -> ManufacturerMainType?
    {
        return self.dataSource[indexPath]
    }
    
}

// MARK: - ManufacturerMainTypesViewModelType implementation
extension ManufacturerMainTypesViewModel: ManufacturerMainTypesViewModelType {
    
    var inputs: ManufacturerMainTypesViewModelInputs { get { return self } }
    var outputs: ManufacturerMainTypesViewModelOutputs { get { return self } }
    
}

//
//  ManufacturerMainTypesRouter.swift
//  CarModels
//
//  Created by Giorgi Iashvili on 9/22/19.
//  Copyright © 2019 Giorgi Iashvili. All rights reserved.
//

import Foundation

protocol ManufacturerMainTypesRouter {
    
    func manufacturer(_ manufacturer: Manufacturer, didChooseMainType mainType: ManufacturerMainType)
    
}

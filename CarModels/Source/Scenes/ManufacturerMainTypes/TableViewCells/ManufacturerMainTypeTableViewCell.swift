//
//  ManufacturerMainTypeTableViewCell.swift
//  CarModels
//
//  Created by Giorgi Iashvili on 9/22/19.
//  Copyright © 2019 Giorgi Iashvili. All rights reserved.
//

import UIKit
import RDExtensionsSwift

class ManufacturerMainTypeTableViewCell: UITableViewCell, Reusable {
    
    @IBOutlet private weak var lblName: UILabel!
    
    func fill(withManufacturerMainType manufacturerMainType: ManufacturerMainType, indexPath: IndexPath)
    {
        self.lblName.text = manufacturerMainType.name
        
        self.contentView.backgroundColor = indexPath.row % 2 == .zero ? .white : .lightGray
    }
    
}

//
//  AppRouter.swift
//  CarModels
//
//  Created by Giorgi Iashvili on 9/22/19.
//  Copyright © 2019 Giorgi Iashvili. All rights reserved.
//

import UIKit

class AppRouter {
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController = .init())
    {
        self.navigationController = navigationController
    }
    
    func load()
    {
        let vc = self.loadManufacturersViewController()
        self.navigationController.setViewControllers([vc], animated: false)
    }
    
    func loadManufacturersViewController() -> ManufacturersViewController
    {
        let service = ManufacturerService()
        let viewModel = ManufacturersViewModel(withService: service)
        let vc = ManufacturersViewController.load(withViewModel: viewModel, router: self)
        return vc
    }
    
    func loadManufacturerMainTypesViewController(withManufacturer manufacturer: Manufacturer) -> ManufacturerMainTypesViewController
    {
        let service = ManufacturerMainTypeService()
        let viewModel = ManufacturerMainTypesViewModel(withService: service, manufacturer: manufacturer)
        let vc = ManufacturerMainTypesViewController.load(withViewModel: viewModel, router: self)
        return vc
    }
    
    func loadAlertController(withManufacturer manufacturer: Manufacturer, mainType: ManufacturerMainType) -> UIAlertController
    {
        let alert = UIAlertController(message: "\(manufacturer.name), \(mainType.name)", actions: [], cancelButtonTitle: "Close")
        return alert
    }
    
}

// MARK: - ManufacturersRouter implementation
extension AppRouter: ManufacturersRouter {
    
    func loadMainTypes(withManufacturer manufacturer: Manufacturer)
    {
        let vc = self.loadManufacturerMainTypesViewController(withManufacturer: manufacturer)
        self.navigationController.pushViewController(vc, animated: true)
    }
    
}

// MARK: - ManufacturerMainTypesRouter implementation
extension AppRouter: ManufacturerMainTypesRouter {
    
    func manufacturer(_ manufacturer: Manufacturer, didChooseMainType mainType: ManufacturerMainType)
    {
        let alert = self.loadAlertController(withManufacturer: manufacturer, mainType: mainType)
        self.navigationController.present(alert, animated: true)
    }
    
}

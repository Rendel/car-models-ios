//
//  Network+Status.swift
//  CarModels
//
//  Created by Giorgi Iashvili on 9/22/19.
//  Copyright © 2019 Giorgi Iashvili. All rights reserved.
//

import Foundation
import SwiftyJSON

extension Network {
    
    class Status: Error, Decodable {
        
        enum Code: Int {
            
            // MARK: - General Codes
            case unknown = 0
            
            case ok = 200
            
            // MARK: - Custom server error codes goes here
            
            // MARK: - Success code values
            static var successCodeValues = (200 ... 299).toArray
        }
        
        static var error: Status { get { return Status(code: .unknown) } }
        static var success: Status { get { return Status(code: .ok) } }
        
        var code = Code.unknown
        private(set) var json: JSON?
        
        static func decode(from data: Data?) -> Self
        {
            if let status = self.decode(from: data)
            {
                return status
            }
            else
            {
                return self.init(code: .unknown)
            }
        }
        
        required init(from decoder: Decoder) throws
        {
            self.code = .ok
            self.json = try JSON(from: decoder)
        }
        
        required init(code: Code, jsonData: Any? = nil)
        {
            self.code = code
            if let jsonData = jsonData
            {
                self.json = JSON(jsonData)
            }
        }
        
    }
    
}

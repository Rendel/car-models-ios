//
//  Network+Query.swift
//  CarModels
//
//  Created by Giorgi Iashvili on 9/22/19.
//  Copyright © 2019 Giorgi Iashvili. All rights reserved.
//

import Foundation

extension Network {
    
    typealias Query = Dictionary<String, String>
    
    static func query(with query: Query = .defaultQuery) -> Query
    {
        let query = query
        // Note: Any custom all request needed query goes here
        return query
    }
    
}

extension Network.Query {
    
    static var defaultQuery: Network.Query { get { return [:] } }
    static var emptyQuery: Network.Query { get { return [:] } }
    
}

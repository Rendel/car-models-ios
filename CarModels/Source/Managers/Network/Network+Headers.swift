//
//  Network+Headers.swift
//  CarModels
//
//  Created by Giorgi Iashvili on 9/22/19.
//  Copyright © 2019 Giorgi Iashvili. All rights reserved.
//

import Foundation

extension Network {
    
    typealias Headers = Dictionary<String, String>
    
    static func headers(with headers: Headers = .defaultHeaders) -> Headers
    {
        let headers = headers
        // Note: Any custom all request needed headers goes here
        return headers
    }
    
}

extension Network.Headers {
    
    static var defaultHeaders: Network.Headers { get { return [:] } }
    static var emptyHeaders: Network.Headers { get { return [:] } }
    
}

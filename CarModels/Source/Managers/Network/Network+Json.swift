//
//  Network+Json.swift
//  CarModels
//
//  Created by Giorgi Iashvili on 9/22/19.
//  Copyright © 2019 Giorgi Iashvili. All rights reserved.
//

import Alamofire

// MARK: - Initialize Request
extension Network {
    
    private static func jsonRequest(url: String, path: String, headers: Headers = .defaultHeaders, query: Query = .defaultQuery, body: Body = .defaultBody, method: HTTPMethod = .get, encoding: ParameterEncoding = URLEncoding.default) -> DataRequest
    {
        // Note: Any custom required modifications for json type request goes here
        return self.request(url: url, path: path, headers: headers, query: query, body: body, method: method, encoding: encoding)
    }
    
}

// MARK: - Execute Request
extension Network {
    
    static func jsonRequest(request: DataRequest, anyResponse: AnyResponseBlock, fail: StatusBlock)
    {
        request.responseJSON(queue: DispatchQueue(label: .uuid)) { response in
            let statusCode = response.response?.statusCode ?? -1
            print("Url: " + (response.response?.url?.absoluteString ?? "-1"))
            print("Status code: " + statusCode.toString)
            switch(response.result)
            {
            case .success(let value):
                print(value)
                DispatchQueue.main.async {
                    anyResponse?(response)
                }
            case .failure(let error):
                print(error)
                print(String(data: response.data ?? Data(), encoding: .utf8) ?? "")
                let status = Status(code: .unknown, jsonData: response.data)
                DispatchQueue.main.async {
                    fail?(status)
                }
            }
        }
    }
}

// MARK: - Request Execute Overloads
extension Network {
    
    static func jsonRequest(url: String, path: String, headers: Headers = .defaultHeaders, query: Query = .defaultQuery, body: Body = .defaultBody, method: HTTPMethod = .get, encoding: ParameterEncoding = URLEncoding.default, anyResponse: AnyResponseBlock, fail: StatusBlock)
    {
        DispatchQueue(label: .uuid).async {
            let request = self.jsonRequest(url: url, path: path, headers: headers, query: query, body: body, method: method, encoding: encoding)
            self.jsonRequest(request: request, anyResponse: anyResponse, fail: fail)
        }
    }
    
    static func jsonRequest(url: String, path: String, headers: Headers = .defaultHeaders, query: Query = .defaultQuery, body: Body = .defaultBody, method: HTTPMethod = .get, encoding: ParameterEncoding = URLEncoding.default, json: JsonBlock, fail: StatusBlock)
    {
        self.jsonRequest(url: url, path: path, headers: headers, query: query, body: body, method: method, encoding: encoding, anyResponse: { response in
            let status = Status.decode(from: response.data)
            if let j = status.json,
                status.code == .ok
            {
                json?(j)
            }
            else
            {
                fail?(status)
            }
        }, fail: fail)
    }
    
    static func jsonRequest<T: Decodable>(url: String, path: String, headers: Headers = .defaultHeaders, query: Query = .defaultQuery, body: Body = .defaultBody, method: HTTPMethod = .get, encoding: ParameterEncoding = URLEncoding.default, type: T.Type, success: ((T) -> Void)? = nil, fail: StatusBlock)
    {
        self.jsonRequest(url: url, path: path, headers: headers, query: query, body: body, method: method, encoding: encoding, anyResponse: { response in
            if let object = T.decode(from: response.data)
            {
                success?(object)
            }
            else
            {
                fail?(.error)
            }
        }, fail: fail)
    }
    
}

//
//  Network.swift
//  CarModels
//
//  Created by Giorgi Iashvili on 9/22/19.
//  Copyright © 2019 Giorgi Iashvili. All rights reserved.
//

import RDExtensionsSwift
import Alamofire
import SwiftyJSON

class Network {
    
    typealias AnyResponseBlock = ((_ response: DataResponse<Any>) -> Void)?
    typealias AnyBlock = ((_ response: Any) -> Void)?
    typealias JsonBlock = ((_ response: JSON) -> Void)?
    typealias StatusBlock = ((_ status: Status) -> Void)?
    
    static let shared = Network()
    
    let alamofire: Alamofire.SessionManager = {
        let manager = Alamofire.SessionManager(
            configuration: URLSessionConfiguration.default,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: [:])
        )
        
        return manager
    }()
    
    init()
    {
        self.alamofire.delegate.sessionDidReceiveChallenge = { [weak self] session, challenge in
            var disposition = URLSession.AuthChallengeDisposition.performDefaultHandling
            var credential: URLCredential?
            switch(challenge.protectionSpace.authenticationMethod)
            {
            case NSURLAuthenticationMethodServerTrust:
                disposition = URLSession.AuthChallengeDisposition.useCredential
                if let secTrust = challenge.protectionSpace.serverTrust
                {
                    credential = URLCredential(trust: secTrust)
                }
            default:
                if(challenge.previousFailureCount > 0)
                {
                    disposition = .cancelAuthenticationChallenge
                }
                else
                {
                    credential = self?.alamofire.session.configuration.urlCredentialStorage?.defaultCredential(for: challenge.protectionSpace)
                    if(credential != nil)
                    {
                        disposition = .useCredential
                    }
                }
            }
            return (disposition, credential)
        }
    }
    
}

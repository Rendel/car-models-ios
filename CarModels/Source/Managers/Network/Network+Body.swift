//
//  Network+Body.swift
//  CarModels
//
//  Created by Giorgi Iashvili on 9/22/19.
//  Copyright © 2019 Giorgi Iashvili. All rights reserved.
//

import Foundation

extension Network {
    
    typealias Body = Dictionary<String, Any>
    
    static func body(with body: Body = .defaultBody) -> Body
    {
        let body = body
        // Note: Any custom all request needed body goes here
        return body
    }
    
}

extension Network.Body {
    
    static var defaultBody: Network.Body { get { return [:] } }
    static var emptyBody: Network.Body { get { return [:] } }
    
}

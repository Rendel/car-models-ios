//
//  Network+Request.swift
//  CarModels
//
//  Created by Giorgi Iashvili on 9/22/19.
//  Copyright © 2019 Giorgi Iashvili. All rights reserved.
//

import Alamofire

extension Network {
    
    static func request(url: String, path: String, headers: Headers = .defaultHeaders, query: Query = .defaultQuery, body: Body = .defaultBody, method: HTTPMethod = .get, encoding: ParameterEncoding = URLEncoding.default) -> DataRequest
    {
        let path = "\(path)?\(query.map { "\($0.key)=\($0.value)" }.joined(separator: "&"))"
        let request = self.shared.alamofire.request(url + path, method: method, parameters: body, encoding: encoding, headers: headers)
        
        print("Url: " + (request.request?.url?.absoluteString ?? "-1"))
        print(headers)
        print(body)
        
        return request.validate(statusCode: Network.Status.Code.successCodeValues)
    }
    
}

//
//  ManufacturersViewModelUnitTests.swift
//  CarModelsUnitTests
//
//  Created by Giorgi Iashvili on 9/22/19.
//  Copyright © 2019 Giorgi Iashvili. All rights reserved.
//

import XCTest

@testable import CarModels

class ManufacturersViewModelUnitTests: XCTestCase {
    
    class MockManufacturerService: ManufacturerService {
        
        override func loadManufacturers(page: Int, pageSize: Int, success: @escaping ([Manufacturer]) -> Void, fail: @escaping (Error) -> Void)
        {
            success((page * pageSize ..< (page * pageSize + pageSize)).map { Manufacturer(id: "\($0)", name: "\($0)") })
        }
        
    }
    
    class MockManufacturersViewModel: ManufacturersViewModel {
        
    }
    
    private var service: MockManufacturerService!
    private let pageSize = 15
    private var viewModel: MockManufacturersViewModel!
    
    override func setUp()
    {
        self.service = MockManufacturerService()
        self.viewModel = MockManufacturersViewModel(withService: self.service, pageSize: self.pageSize)
    }
    
    func testLoadManufacturersOnePage()
    {
        let expectation = self.expectation(description: "Test Load Manufacturers One Page")
        self.viewModel.inputs.loadManufacturers(page: 0, success: {
            XCTAssertEqual(1, self.viewModel.outputs.numberOfSections(), "Sections should be 1")
            XCTAssertEqual(self.pageSize, self.viewModel.outputs.numberOfItems(inSection: .zero), "Rows should be \(self.pageSize)")
            let firstManufacturer = self.viewModel.outputs.item(atIndexPath: [.zero, .zero])
            XCTAssertTrue(firstManufacturer != nil, "First Manufacturer should not be nil")
            XCTAssertEqual("0, 0", "\(firstManufacturer!.id), \(firstManufacturer!.name)", "First Manufacturer id and name should be 0, 0")
            expectation.fulfill()
        }, fail: { error in
            XCTAssertTrue(false, error.localizedDescription)
        })
        
        self.waitForExpectations(timeout: 5) { error in
            XCTAssertTrue(error == nil, error!.localizedDescription)
        }
    }
    
    func testLoadManufacturersMultiplePages()
    {
        let expectation = self.expectation(description: "Test Load Manufacturers Multiple Pages")
        self.viewModel.inputs.loadManufacturers(page: 0, success: {
            self.viewModel.inputs.loadManufacturers(page: 1, success: {
                XCTAssertEqual(1, self.viewModel.outputs.numberOfSections(), "Sections should be 1")
                XCTAssertEqual(self.pageSize * 2, self.viewModel.outputs.numberOfItems(inSection: .zero), "Rows should be \(self.pageSize)")
                let lastManufacturer = self.viewModel.outputs.item(atIndexPath: [.zero, 29])
                XCTAssertTrue(lastManufacturer != nil, "Last Manufacturer should not be nil")
                XCTAssertEqual("29, 29", "\(lastManufacturer!.id), \(lastManufacturer!.name)", "Last Manufacturer id and name should be 29, 29")
                expectation.fulfill()
            }, fail: { error in
                XCTAssertTrue(false, error.localizedDescription)
            })
        }, fail: { error in
            XCTAssertTrue(false, error.localizedDescription)
        })
        
        self.waitForExpectations(timeout: 5) { error in
            XCTAssertTrue(error == nil, error!.localizedDescription)
        }
    }
    
}

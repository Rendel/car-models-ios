//
//  ManufacturerMainTypesViewModelUnitTests.swift
//  CarModelsUnitTests
//
//  Created by Giorgi Iashvili on 9/22/19.
//  Copyright © 2019 Giorgi Iashvili. All rights reserved.
//

import XCTest

@testable import CarModels

class ManufacturerMainTypesViewModelUnitTests: XCTestCase {
    
    class MockManufacturerMainTypeService: ManufacturerMainTypeService {
        
        let itemsCount: Int
        
        init(itemsCount: Int)
        {
            self.itemsCount = itemsCount
            
            super.init()
        }
        
        override func loadManufacturerMainTypes(manufacturerId: String, success: @escaping ([ManufacturerMainType]) -> Void, fail: @escaping (Error) -> Void)
        {
            success((0 ..< 10).map { ManufacturerMainType(name: "\($0)") })
        }
        
    }
    
    class MockManufacturerMainTypesViewModel: ManufacturerMainTypesViewModel {
        
    }
    
    private let itemsCount = 10
    private var service: MockManufacturerMainTypeService!
    private var manufacturer: Manufacturer!
    private var viewModel: MockManufacturerMainTypesViewModel!
    
    override func setUp()
    {
        self.service = MockManufacturerMainTypeService(itemsCount: self.itemsCount)
        self.manufacturer = Manufacturer(id: "0", name: "0")
        self.viewModel = MockManufacturerMainTypesViewModel(withService: self.service, manufacturer: self.manufacturer)
    }
    
    func testLoadManufacturerMainTypes()
    {
        let expectation = self.expectation(description: "Test Load Manufacturer Main Types")
        self.viewModel.inputs.loadManufacturerMainTypes(success: {
            XCTAssertEqual(1, self.viewModel.outputs.numberOfSections(), "Sections should be 1")
            XCTAssertEqual(self.itemsCount, self.viewModel.outputs.numberOfItems(inSection: .zero), "Rows should be \(self.itemsCount)")
            let firstManufacturerMainType = self.viewModel.outputs.item(atIndexPath: [.zero, .zero])
            XCTAssertTrue(firstManufacturerMainType != nil, "First Manufacturer Main Type should not be nil")
            XCTAssertEqual("0", "\(firstManufacturerMainType!.name)", "First Manufacturer Main Type name should be 0")
            expectation.fulfill()
        }, fail: { error in
            XCTAssertTrue(false, error.localizedDescription)
        })
        
        self.waitForExpectations(timeout: 5) { error in
            XCTAssertTrue(error == nil, error!.localizedDescription)
        }
    }
    
}

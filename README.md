Car Models Application:

To build & run the project:
1. Open terminal go to CarModels home directory and run command: 'pod install'
2. Open CarModels.xcworkspace
3. (Ignore this step if you want to run application on simulator) Open project settings, select CarModels tagert and choose the Development Team. Repeat the same for CarModelsUnitTests target.
4. Build, Run, Test & Enjoy

Architectural design pattern used:
MVVM

3rd party libraries used:
1. RDExtensionsSwift: This library is written by me (Giorgi Iashvili). It is a collection of useful extensions for Swift. It makes your iOS app development much easier and lets you to write simplier and cleaner code.
2. Alamofire: It is an HTTP networking library written in Swift.
3. SwiftyJSON: It makes it easy to deal with JSON data in Swift.
4. SVProgressHUD: A simple loader to display the progress of an ongoing task
